# Lab4_Task2

Эскиз задания о последовательности, нужно докончить

#include <iostream>
const int M = 1000;
int main()
{
    int a[M];
    int n, i, j;
    int f[M];
    int max[M];
    //Ввод количества чисел в последовательности
    std::cin >> n;
    //Ввод чисел
    for (i = 0; i < n; i++)
        std::cin >> a[i];
    for (i = 0; i < n - 1; i++)
        for (j = 0; j < n; j++)
        {
            //Определение первой цифры числа
            int p = a[i];
            f[i] = 0;
            while (p > 9)
            {
                p /= 10;
                f[i] = p % 10;
            }
            int temp = 0;
            max[i] = 0;
            //Определение наибольшей цифры числа
            while (p > 9)
            {
                if (p % 10 > max[i])
                    max[i] = p % 10;
                p /= 10;
            }
            max[i] = temp;
            Сравнение чисел по неубыванию первых цифр, наибольших цифр и, наконец, самих чисел
            if (f[i] != f[j])
            {
                if (f[i] > f[j])
                {
                    temp = a[i];
                    a[i] = a[j];
                    a[j] = temp;
                }
            }
            else if (max[i] != max[j])
            {
                if (max[i] > max[j])    
                {    
                    temp = a[i];    
                    a[i] = a[j];    
                    a[j] = temp;    
                }
            }
            else if (a[i] > a[j])    
            {        
            temp = a[i];            
            a[i] = a[j];            
            a[j] = temp;            
            }
        }
    for (i = 0; i < n; i++)
        std::cout << a[i] << " ";
    return 0;
}
